public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());
        Labrador l = new Labrador("Carlo","Red");
        System.out.println(l);
        System.out.println(l.speak());
        Yorkshire y = new Yorkshire("Franco","Yellow");
        System.out.println(y);
        System.out.println(y.speak());

    }
}
