/**
 Yorkshire.java
 A class derived from Dog that holds information about
 a Yorkshire terrier. Overrides Dog speak method.
@ TODO this file is largely incomplete

 */

public class Yorkshire extends Dog
{
    private String color; //black, yellow, or chocolate?
    private static int breedWeight = 75;

    public Yorkshire(String name, String color)
    {
        super(name);
        this.color = color;
    }

    /**
     * Small bark -- overrides speak method in Dog
     * @return a small bask string
     */

    public String speak()
    {
        return "Bau Bau";
    }
    public static int avgBreedWeight()
    {
        return breedWeight;
    }
    public String toString() {return "Color: "+color+", Weight: "+breedWeight+", Name: "+name;}
}